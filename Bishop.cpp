// Nicolas Barrera, nbarrer1
// Rashi Bhatt, rbhatt6
// Andy Chen, achen93

#include "Bishop.h"
#include <cmath>

/* Indicates whether the given start and end coordinates describe
 * a move that would be considered valid for this piece if there
 * is no piece at the end position (i.e. this is not a capture move).
 * Makes sure the shape is diagonal in any direction for bishop. 
 */
bool Bishop::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  // Define columns and rows and distances between them.
  int start_col = start.first;
  int start_row = start.second;
  int end_col = end.first;
  int end_row = end.second; 
  int dx = abs(end_col - start_col);
  int dy = abs(end_row - start_row);
  
  // When traversing diagonally with no limits,
  // the change in x and y should be equal.
  if (dx == dy) {
    return true;
  } else {
    return false;
  }
}
