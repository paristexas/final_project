// Nicolas Barrera, nbarrer1
// Rashi Bhatt, rbhatt6
// Andy Chen, achen93

#include <iostream>
#include <utility>
#include <map>
#include <ctype.h>
#include "Board.h"
#include "CreatePiece.h"


/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Board::Board(){}

/*
 * Destructor that checks every position for a piece, deletes that
 * piece object, and then clears the pointer from the map.
 */ 
Board::~Board() {
  // For every single board position,
  for(char r = '8'; r >= '1'; r--) {
    for(char c = 'A'; c <= 'H'; c++) {
      std::pair<char,char> position = std::pair<char, char>(c,r);
      // if there is a piece there,
      if (this->operator()(position)) {
	// delete the piece and erase the pointer from the map.
	delete this->operator()(position);
	occ.erase(position);
      }
    }
  }
}

/*
 * Returns a const pointer to the piece at a prescribed location 
 * if it exists using the occ map, or nullptr if there is nothing there.
 */
const Piece* Board::operator()(std::pair<char, char> position) const {
  // Checks if there is a Piece* in that map position.
  // If not, return NULL to prevent out-of-range error.
  if (occ.count(position) == 0) {
    return NULL;
  }

  // Return the Piece* at that position
  return occ.at(position);
}

/*
 * Helper function for add_piece() to check validity
 * of the piece_designator value.
 */
bool Board::check_value(char piece_designator) {
  switch (piece_designator) {
  case 'K': return true;
  case 'k': return true;
  case 'Q': return true;
  case 'q': return true;
  case 'B': return true;
  case 'b': return true;
  case 'N': return true;
  case 'n': return true;
  case 'R': return true;
  case 'r': return true;
  case 'P': return true;
  case 'p': return true;
  case 'M': return true;
  case 'm': return true;
  default: return false;
  }
}

/* Attempts to add a new piece with the specified designator, at the given location.
 * Returns false if:
 * -- the designator is invalid,
 * -- the specified location is not on the board, or
 * -- if the specified location is occupied
 */
bool Board::add_piece(std::pair<char, char> position, char piece_designator) {
  // Check if piece_designator is a valid char input.
  if (!check_value(piece_designator)) {
    return false;
  }

  // Check if the specified location is within bounds on the board.
  if ('H' < position.first || position.first < 'A' || '8' < position.second || position.second < '1') {
    return false;
  }
  // If position is already occupied/exists:
  if (occ.count(position) == 1) {
    return false;
  }
  // If able to be added, create a new key-value for that position
  // with a mapped-value for the new Piece* returned by create_piece.
  occ[position] = create_piece(piece_designator);
  return true;
}


/*
 * Helper function to delete piece and adjust occ map by erasing the pointer
 * of that specific piece from the occ map.
 */
void Board::delete_piece(std::pair<char, char> position) {
  // Delete the piece at that position.
  delete this->operator()(position);
  // Removes the pointer at that position
  // from the map.
  occ.erase(position);
}

/*
 * Iterates through the board and counts number of kings.
 * Returns true if the board has the right number of kings on it
 */
bool Board::has_valid_kings() const {
  int count = 0;
  // Iterate through all elements of the board --
  // If there is a piece in that position, convert
  // to ascii and add to count if it is a King.
  for(char r = '8'; r >= '1'; r--) {
    for(char c = 'A'; c <= 'H'; c++) {
      const Piece* piece = this->operator()(std::pair<char, char>(c, r));
      if (piece) {
        if (piece->to_ascii() == 'K' || piece->to_ascii() == 'k') {
          count++;
        }
      }
    }
  }
  // Returns bool value based on whether there are 2 kings or not
  return count == 2;
}

/** Maybe just use the board, because this requires 5 overloaded functions.
 * Helper function for Chess.cpp's in_check function.
 * Return the char pair of the king's position.
 */
std::pair<char, char> Board::find_king(bool white) const {
  /**
  // definite a iterator for the map.
  std::map<std::pair<char, char>, Piece*>::iterator it;
  // iterate through the occ map
  for (it = occ.begin(); it != occ.end(); it++) {
    // If the piece is a king, and its color is the same color
    // indicated by the passed in bool value, return that
    // position char pair
    if (tolower(it->second->to_ascii()) == 'k' && it->second->is_white() == white) {
      return it->first;
    }
  }
  **/
  //iterates through the board
  std::pair<char,char> location;
  for (char r = '8'; r>= '1'; r--) {
    for (char c = 'A'; c<= 'H'; c++) {
      //finds a piece
      const Piece* piece = this->operator()(std::pair<char, char>(c, r));
      if (piece) {
	//Checks whose turn it is
	if (white) {
	  if (piece->to_ascii() == 'K') {
	    location = std::make_pair(c, r);
	  }
	}
	if (!white) {
	  if (piece->to_ascii() == 'k') {
	    location = std::make_pair(c, r);
	  }
	}
      }
    }
  }
  return location;
}

/*
 * Displays the board by printing it to stdout. Uses Terminal.h
 * to customize colors of the foreground.
 */
void Board::display() const {
  // Change foreground color for the text introducing board
  // and the board itself, then set back to the default.
  Terminal::color_fg(true, Terminal::WHITE);
  std::cout << "\nHere is your current board!\n" << std::endl;
  Terminal::color_fg(true, Terminal::BLACK);
  // Display the column letters to help the user when
  // making move.
  std::cout << "ABCDEFGH" << std::endl;
  Terminal::color_fg(false, Terminal::RED);
  std::cout << *this << std::endl;
  Terminal::set_default();
}


/*
 * Write the board state to an output stream by iterating through
 * board, checking if there is a piece there, and then printing the
 * letter of the piece, or a dash if no piece.
 */
/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator<<(std::ostream& os, const Board& board) {
  for(char r = '8'; r >= '1'; r--) {
    for(char c = 'A'; c <= 'H'; c++) {
      const Piece* piece = board(std::pair<char, char>(c, r));
      if (piece) {
	os << piece->to_ascii();
      } else {
	os << '-';
      }
    }
    os << std::endl;
  }
  return os;
}

