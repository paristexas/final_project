// Nicolas Barrera, nbarrer1
// Rashi Bhatt, rbhatt6
// Andy Chen, achen93

#include "Chess.h"

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////

/*
 * A default constructors that initializes board with standard positions.
 * Sets state to white's turn/
 */
Chess::Chess() : is_white_turn(true) {
  // Add the pawns
  for (int i = 0; i < 8; i++) {
    board.add_piece(std::pair<char, char>('A' + i, '1' + 1), 'P');
    board.add_piece(std::pair<char, char>('A' + i, '1' + 6), 'p');
  }

  // Add the rooks
  board.add_piece(std::pair<char, char>( 'A'+0 , '1'+0 ) , 'R' );
  board.add_piece(std::pair<char, char>( 'A'+7 , '1'+0 ) , 'R' );
  board.add_piece(std::pair<char, char>( 'A'+0 , '1'+7 ) , 'r' );
  board.add_piece(std::pair<char, char>( 'A'+7 , '1'+7 ) , 'r' );

  // Add the knights
  board.add_piece(std::pair<char, char>( 'A'+1 , '1'+0 ) , 'N' );
  board.add_piece(std::pair<char, char>( 'A'+6 , '1'+0 ) , 'N' );
  board.add_piece(std::pair<char, char>( 'A'+1 , '1'+7 ) , 'n' );
  board.add_piece(std::pair<char, char>( 'A'+6 , '1'+7 ) , 'n' );

  // Add the bishops
  board.add_piece(std::pair<char, char>( 'A'+2 , '1'+0 ) , 'B' );
  board.add_piece(std::pair<char, char>( 'A'+5 , '1'+0 ) , 'B' );
  board.add_piece(std::pair<char, char>( 'A'+2 , '1'+7 ) , 'b' );
  board.add_piece(std::pair<char, char>( 'A'+5 , '1'+7 ) , 'b' );

  // Add the kings and queens
  board.add_piece(std::pair<char, char>( 'A'+3 , '1'+0 ) , 'Q' );
  board.add_piece(std::pair<char, char>( 'A'+4 , '1'+0 ) , 'K' );
  board.add_piece(std::pair<char, char>( 'A'+3 , '1'+7 ) , 'q' );
  board.add_piece(std::pair<char, char>( 'A'+4 , '1'+7 ) , 'k' );
}

/* 
 * Copy constructor of board
 */
Chess::Chess(const Chess& temp) {
  board = temp.get_board();
  is_white_turn = temp.is_white_turn;
}

/*
 * Helper function to check if user-specified start and end move
 * are valid movements. It checks whether the moves are out-of-bounds,
 * and for non-existing start positions, attempts to move opposite
 * team pieces, and non-movements. Returns true if valid.
 */
bool Chess::check_if_valid(std::pair<char, char> start, std::pair<char, char> end) const {
  // Check if the specified location is within bounds on the board.
  if ('H' < start.first || start.first < 'A' || '8' < end.second || end.second < '1') {
    //std::cout << "'What is not in reach is not for you.' Desired position is out of bounds." << std::endl;
    return false;
  }

  // If piece doesn't exist at start
  // position, return false.
  if (!board(start)) {
    //std::cout << "Cannot move a non-existent piece." << std::endl;
    return false;
  }

  // If the user is trying to move a piece that's not theirs,
  // return false.
  if ((is_white_turn && !board(start)->is_white()) || (!is_white_turn && board(start)->is_white())) {
    //std::cout << "Cheaters never prosper! Cannot move opponent's piece." << std::endl;
    return false;
  }
  
  // Check if start and end positions are the same.
  if (start.first == end.first && start.second == end.second) {
    //std::cout << " 'Rocks that never move never prosper.' Cannot move piece to the same location." << std::endl;
    return false;
  }

  return true;
}

/*
 * Helper function that checks if there are any pieces between the start and
 * end position. Called after move and capture shape are found to be legal.
 * Returns true if there are pieces in between.
 */
bool Chess::check_in_between(std::pair<char, char> start, std::pair<char, char> end) const {
  // Define distances between columns and rows
  int total_dx = end.first - start.first;
  int total_dy = end.second - start.second;
  
  // distances to iterate through
  int dx;
  int dy;
  
  // If start column is less than end column,
  // that means the piece is moving right.
  // Otherwise, it is moving left
  if (start.first < end.first) {
    dx = 1;
  } else if (start.first > end.first) {
    dx = -1;
  }
  
  // If start row is greater than end row,
  // that means the piece is moving up,
  // otherwise it is moving down
  if (start.second < end.second) {
    dy = 1;
  } else if (start.second > end.second) {
    dy = -1;
  }

  // Starting col and row variables to iterate through
  char current_col = start.first + dx;
  char current_row = start.second + dy;
  
  //  Moving forward and backwards: Queen, Rook, Pawn
  if (total_dx == 0) {
    // While the current row is not the end row,
    // iterate up/down, and see if there is a piece there.
    while (current_row != end.second) {
      if (board(std::pair<char, char>(end.first, current_row))) {
	return true;
      }
      current_row += dy; 
    }
  // Moving left and right: Queen & rook
  } else if (total_dy == 0) {
    // While the current column is not the end column,
    // iterate left/right and see if there is a piece there.
    while (current_col != end.first) {
      if (board(std::pair<char,char>(current_col, end.second))) {
	return true;
      }
      current_col += dx;
    }
  // Moving diagonally: Queen & Bishop
  } else if (abs(total_dx) == abs(total_dy)) {
    while(current_col != end.first) {
      if (board(std::pair<char,char>(current_col, current_row))) {
	return true;
      }
      current_col += dx;
      current_row += dy;
    }
  }
  return false;
}

/*
 * Attempts to make move, and makes sure each moves are properly specified,
 * that the move and capture shapes are valid, and other conditions.
 * Returns true if move can be made.
 */
bool Chess::make_move(std::pair<char, char> start, std::pair<char, char> end) {
  // Use helper function and see if the move is properly
  // specified first.
  if(!check_if_valid(start, end)) { 
    return false;
  }
  
  // If there is a piece at the end position,
  // check if piece can be captured.
  if (board(end)) {
    if (check_in_between(start, end)) {
      //std::cout << "There are pieces in between your start and end move!" << std::endl;
      return false;
    }
    // First check if the shape of the capture can be done and there are no pieces in between
    if (board(start)->legal_capture_shape(start, end)) {
      char previous_start = board(start)->to_ascii();
      char previous_end = board(end)->to_ascii();
      if ((board(end)->is_white() && board(start)->is_white()) || (!board(end)->is_white() && !board(start)->is_white())) {
	//std::cout << "'You are your own worst enemy.' Cannot capture your own piece." << std::endl;
        return false;
	// Otherwise, it can be captured!
      } else {
        // Delete memory allocated for the piece
        // at end position and erase pointer from occ map.
        board.delete_piece(end);
        // In addition to capturing the piece at the end location,
        // check if piece is a pawn and is entering the opponent's territory. 
        // If so, promote the pawn to queen.
        if (board(start)->to_ascii() == 'P' && end.second == '8') {
          board.add_piece(end, 'Q');
        } else if (board(start)->to_ascii() == 'p' && end.second == '1') {
          board.add_piece(end, 'q');
        } else {
	  // Otherwise, if not a pawn, just add using the start
	  // piece ascii value.
          board.add_piece(end, board(start)->to_ascii());
        }
        // Delete start piece, switch turns, and return true.
        board.delete_piece(start);

	// However, if it is now in check, the move is not valid.
        if (in_check(is_white_turn)) {
	  // delete the piece that just moved
          board.delete_piece(end);
	  // readd the piece that was captured
          board.add_piece(end, previous_end);
	  // readd the piece to the start
          board.add_piece(start, previous_start);
          // Can't make the move.
          //std::cout << "Can't make this move. Will put you in checkmate." << std::endl;
          return false;
        }
        is_white_turn = !is_white_turn;
        return true;
      }
      // If legal capture cannot be done, or there are pieces
      // in between, return false.
    } else {
      //std::cout << "Not a legal capture." << std::endl;
      return false;
    }
    // If no piece at the end position, just check legal move shape.
  } else if (board(start)->legal_move_shape(start, end)) {
    char previous_start = board(start)->to_ascii();
    if (check_in_between(start, end)) {
      //std::cout << "There are pieces in between your start and end move!" << std::endl;
      return false;
    }
    
    if (board(start)->to_ascii() == 'P' && end.second == '8') {
	board.add_piece(end, 'Q');
      } else if (board(start)->to_ascii() == 'p' && end.second == '1') {
	board.add_piece(end, 'q');
      } else {
	board.add_piece(end, board(start)->to_ascii());
      }
      board.delete_piece(start);
      // If it is in check now, then the move is invalid.
      if (in_check(is_white_turn)) {
        board.delete_piece(end);
        board.add_piece(start, previous_start);
        // Can't make the move.
        //std::cout << "Can't make this move. Will put you in checkmate." << std::endl;
        return false;
     }
      is_white_turn = !is_white_turn;
      return true;
      // Return false if legal move cannot be made.
  } else {
    //std::cout << "Not a legal move." << std::endl;
      return false;
  }
}

/*
 * Returns false if the new move causes player to be in check.
 * Otherwise, returns true if new wmove doesn't cause in check.
 */
bool Chess::new_check(bool white, std::pair<char, char> start, std::pair<char, char> end) {
  
  char previous_start = board(start)->to_ascii();
 
  if (board(end)) {
    char previous_end = board(end)->to_ascii();
    board.delete_piece(end);
    board.add_piece(end, board(start)->to_ascii());
    board.delete_piece(start);
    if (in_check(white)) {
      board.delete_piece(end);
      board.add_piece(end, previous_end);
      board.add_piece(start, previous_start);
      // Can't make the move.
      std::cout << "Can't make this move. Will put you in checkmate." << std::endl;
      return false;
    } else {
      // Move is possible. 
      return true;
    }
  } else {
    board.add_piece(end, board(start)->to_ascii());
    board.delete_piece(start);
    if (in_check(white)) {
      board.delete_piece(end);
      board.add_piece(start, previous_start);
      // Can't make the move.
      std::cout << "Can't make this move. Will put you in checkmate." << std::endl;
      return false;
    } else {
      // Move is possible. 
      return true;
    }
  }
}

/*
 * Returns true if the designated player is in check.
 */
bool Chess::in_check(bool white) const {
  // Define king_position using helper function
  std::pair<char,char> king_position = board.find_king(white);
  // Iterate through the board
  for (char r = '8'; r >= '1'; r--) {
    for (char c = 'A'; c <= 'H'; c++) {
      // If there is a piece that is the opposite color,
      if (board(std::pair<char,char>(c,r))) {
	if (board(std::pair<char,char>(c,r))->is_white() == !white) {
	  std::pair<char,char> start(c,r);
	  // Check if it can capture the king and that there are no pieces
	  // in between. Return true, because the player currently is in check.
	  if (!check_in_between(start, king_position) && board(start)->legal_capture_shape(start,king_position))
	    return true;
	}
      }
    }
  }
  // If nothing can capture the king, it is not in check.
  return false;
}

/*
 * Helper function that determines if the next move that the King
 * tries to make places itself in a checked position.
 */
bool Chess::if_next_move_places_check(bool white, std::pair<char, char> end) const {
  // Define king_position using helper function. 
  std::pair<char,char> new_king_position = end; 
  // Iterate through the board
  for (char r = '8'; r >= '1'; r--) {
    for (char c = 'A'; c <= 'H'; c++) {
      // If there is a piece that is the opposite color,
      if (board(std::pair<char,char>(c,r))) {
        if (board(std::pair<char,char>(c,r))->is_white() == !white) {
          std::pair<char,char> start(c,r);
          // Check if it can capture the king and that there are no pieces
          // in between. 
          // If so, return true because the player will be in check.
          if (!check_in_between(start, new_king_position) && board(start)->legal_capture_shape(start, new_king_position)) {
            return true;
          } 
        }
      }
    }
  }
  // Otherwise, nothing is able to capture the King,
  // and the new position is viable.
  return false;
}

/*
 * Returns true if the designated player is in mate.
 */
bool Chess::in_mate(bool white) {
  /**
  // If the player is not currently in check,
  // it cannot be a checkmate.
  if (!in_check(white)) {
    return false;
  }
  char end_char;
  // Iterate through the board
  for (char r = '8'; r >= '1'; r--) {
    for (char c = 'A'; c <= 'H'; c++) {
      // If there is a piece there, and it is same color
      // as the current player's team, define that as the start
      if (board(std::pair<char,char>(c,r))) {
	if (board(std::pair<char,char>(c,r))->is_white() == white) {
	  std::pair<char,char> start(c,r);
	  char start_char = board(start)->to_ascii();
	  // Then iterate through the board again, and make all possible legal
	  // moves and captures, and check if it's still in check.
	  // If it's not in check, then that means there's a possible move
	  // to get out of check, so not a checkmate!
	  for (char r2 = '8'; r2 >= '1'; r2--) {
	    for (char c2 = 'A'; c2 <= 'H'; c2++) {
	      bool capture = false;
	      std::pair<char,char> end(c2,r2);
	      // If there's a piece there, at the end
	      // extract the char, and define capture
	      // to be true.
<<<<<<< HEAD
	      char end_char;
	      if (board(end) && board(end)->is_white() != white) {
=======
	      if (board(end)) {
>>>>>>> 58cd91df7b611778c37a3add8d5c7f2ed5760684
		end_char = board(end)->to_ascii();
		capture = true;
	      }
	      // If make_move suceeds, that means the move got
	      // it out of check, so it is not a checkmate.
	      if (make_move(start,end)) {
		// return board to original.
		// delete the piece that moved to
		// the end
		board.delete_piece(end);
		if (capture == true) {
		  // add in piece that was captured
		  board.add_piece(end, end_char);
		}
		// return starting piece back
		board.add_piece(start, start_char);
		return false;
	      }
	    }
	  }
	}
      }
    }
  }
  **/
  // if no moves were made that got it out of check,
  // then it is a checkmate.
  return false;
}
  /**
  // (DONE) Need to re-check if still in-check after moving.
  // TODO: Need to consider diagonals.

  char Kc;
  char Kr;
  int count = 0;
  int rowcount = 0;
  int colcount = 0;
  std::pair<char,char> possible_position;
  std::pair<char,char> king_position = board.find_king(white);
  Kc = std::get<0>(king_position);
  Kr = std::get<1>(king_position);
  if (in_check(white)) {
    //check if rows are inbounds
    if (Kr - 1 >= '1') { 
      int c = Kc;
      int r = Kr-1; 
      possible_position = std::make_pair(c, r);
      if (board(king_position)->legal_capture_shape(king_position, possible_position) && !if_next_move_places_check(white, possible_position)) {
        count++; 
      }
    } if (Kr + 1 <= '8') {
      int c = Kc;
      int r = Kr+1; 
      possible_position = std::make_pair(c, r);
      if (board(king_position)->legal_capture_shape(king_position, possible_position) && !if_next_move_places_check(white, possible_position)) {
        count++; 
      }
    }
    if (Kc - 1 >= 'A') {
      int r = Kr;
      int c = Kc + 1; 
      possible_position = std::make_pair(c, r);
      if (board(king_position)->legal_capture_shape(king_position, possible_position) && !if_next_move_places_check(white, possible_position)) {
        count++; 
      }
    }
    if ( Kc + 1 <= 'H') {
      int r = Kr;
      int c = Kc + 1; 
      possible_position = std::make_pair(c, r);
      if (board(king_position)->legal_capture_shape(king_position, possible_position) && !if_next_move_places_check(white, possible_position)) {
        count++; 
      }
    }



  //   //check if rows are inbounds
  //   if (Kr - 1 >= '1' || Kr + 1 <= '8') {
  //     int c = Kc;
  //     //iterate through possible moves
  //     for (int r = Kr - 1; r <= Kr + 1; r++) {
  //       possible_position = std::make_pair(c, r);
	// // check if king can move and if it places in check
	// if (board(king_position)->legal_capture_shape(king_position, possible_position) && if_next_move_places_check(white, possible_position)) {
	//   return false;
  //   count++;
	// }
	// rowcount++;
  //     }
  //     //if loopcount = count, no moves in same column are allowed
  //   } else if (Kc - 1 >= 'A' || Kc + 1 <= 'H') {
  //     int r = Kr;
  //     for (int c = Kc - 1; c <= Kc + 1; c++) {
  //       possible_position = std::make_pair(c, r);
  //       if (board(king_position)->legal_capture_shape(king_position, possible_position) && if_next_move_places_check(white, possible_position)) {
  //         return false;
  //   count++;
	// }
	// colcount++;
  //     }
  //   }
  //   int total = colcount + rowcount;
  //   //if king cannot move out of check, check if piece can move to block king  
  //   if (count == total) {
  //     //new part of method to check for other piece movemnt
  //   }
    if (count != 0) {
      return false;
    } else {
      // If all directions lead to a check, player cannot move King out of checkmate. 
      return true;
    }
  }
  else { 
    return false;
<<<<<<< HEAD
  }
}
=======
>>>>>>> 58cd91df7b611778c37a3add8d5c7f2ed5760684
  **/

/*
 * Returns true if the designated player is in stalemate.
 */
bool Chess::in_stalemate(bool white) const {
  // iterate through board
  if (in_check(white)) {
    return false;
  }
  for (char r = '8'; r >= '1'; r--) {
    for (char c = 'A'; c <= 'H'; c++) {
      // If there is a piece there, and it is same color
      // as the current player's team, define that as the start
      if (board(std::pair<char,char>(c,r))) {
	if (board(std::pair<char,char>(c,r))->is_white() == white) {
	  std::pair<char,char> start(c,r);
	  // then iterate through the board again, and see if
	  // that piece at start can make any legal move or capture
	  // the other pieces.
	  for (char r2 = '8'; r2 >= '1'; r2--) {
	    for (char c2 = 'A'; c2 <= 'H'; c2++) {
	      std::pair<char,char> end(c2,r2);
	      // If there's a piece there, and it is the opposite color
	      // see if it can be captured
	      if (board(std::pair<char,char>(c2,r2))) {
		if (board(std::pair<char,char>(c2,r2))->is_white() != white) {
		  // Check if a legal capture can be made and that there are no
		  // pieces in between, return false immediately if so, because not in
		  // stalemate.
		  if (!check_in_between(start, end) && board(start)->legal_capture_shape(start,end)) {
		    return false;
		  }
		}
		// if there is no piece there, check if a move can be made there.
		// and return false, because not in stalemate if move can be made.
	      } else {
		if (!check_in_between(start, end) && board(start)->legal_move_shape(start, end)) {
		  return false;
		}
	      }
	    }
	  }
	}
      }
    }
  }
  // if no legal moves are found it's a stalemate.
  return true;
}

/*
 * Helper function for the >> overloading that sets the bool for
 * the is_white_turn depending on the char on the ninth line.
 */
void Chess::whose_turn(char color) {
  if (color == 'w') {
    is_white_turn = true;
  } else if (color == 'b') {
    is_white_turn = false;
  }
}


/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
/*
 * Writes board out to a stream using the board getter function and
 * adds a 'w' or 'b' to last line depending on the current color turn.
 */
std::ostream& operator<< (std::ostream& os, const Chess& chess) {
  // Write the board out and then either the character 'w' or the character 'b',
  // depending on whose turn it is
  return os << chess.get_board() << (chess.turn_white() ? 'w' : 'b');
}

/*
 * Reads the board in from a stream and deletes and adds pieces accordingly.
 * Also sets the bool of the is_white_turn based on the file's last line.
 */
std::istream& operator>> (std::istream& is, Chess& chess) {
  // Define a char for each piece on the board.
  char piece;
  // Iterate through all board positions.
  for (char r = '8'; r >= '1'; r--) {
    for (char c = 'A'; c <= 'H'; c++) {
      // Read in the char in that position.
      is >> piece;
      // If there is a piece already there, delete that pieces.
      if (chess.get_board()(std::pair<char,char>(c,r))) {
	chess.change_board().delete_piece(std::pair<char,char>(c,r));
      }
      // If the char read in is a letter, then add that piece to the board.
      if (isalpha(piece)) {
	chess.change_board().add_piece(std::pair<char,char>(c, r), piece);
      }
    }
  }
  // Read in the char in the last line
  // and set the boolean of is_white_turn accordingly.
  char color;
  is >> color;
  chess.whose_turn(color);
  return is;
}
