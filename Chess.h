// Nicolas Barrera, nbarrer1
// Rashi Bhatt, rbhatt6
// Andy Chen, achen93

#ifndef CHESS_H
#define CHESS_H

#include <iostream>
#include "Piece.h"
#include "Board.h"

class Chess {

public:
  // This default constructor initializes a board with the standard
  // piece positions, and sets the state to white's turn
  Chess();

  // Returns a constant reference to the board 
  /////////////////////////////////////
  // DO NOT MODIFY THIS FUNCTION!!!! //
  /////////////////////////////////////
  const Board& get_board() const { return board; }

  // Returns a reference to the board, but for the purpose
  // of editing the board (adding and deleting pieces).
  Board& change_board() { return board; }
  
  // Returns true if it is white's turn
  /////////////////////////////////////
  // DO NOT MODIFY THIS FUNCTION!!!! //
  /////////////////////////////////////
  bool turn_white() const { return is_white_turn; }

  // Returns true if a user-specified move is valid.
  bool check_if_valid(std::pair<char, char> start, std::pair<char, char> end) const;

  // Returns true if there are pieces between start and end position.
  bool check_in_between(std::pair<char, char> start, std::pair<char, char> end) const; 

  // Attemps to make a move. If successful, the move is made and
  // the turn is switched white <-> black
  bool make_move(std::pair<char, char> start, std::pair<char, char> end);

  bool if_next_move_places_check(bool white, std::pair<char, char> end) const;

  bool new_check(bool white, std::pair<char, char> start, std::pair<char, char> end);

  // Returns true if the designated player is in check
  bool in_check(bool white) const;

  // Returns true if the designated player is in mate
  bool in_mate(bool white);

  // Returns true if the designated player is in mate
  bool in_stalemate(bool white) const;

  bool if_piece_can_block(bool white, std::pair<char,char> end) const;
  // Helper function for >> overloading to set is_white_turn based
  // on the last char in the file.
  void whose_turn(char color);

private:
  // The board
  Board board;

  // Is it white's turn?
  bool is_white_turn;

};

// Writes the board out to a stream
std::ostream& operator<< (std::ostream& os, const Chess& chess);

// Reads the board in from a stream
std::istream& operator>> (std::istream& is, Chess& chess);


#endif // CHESS_H
