// Nicolas Barrera, nbarrer1
// Rashi Bhatt, rbhatt6
// Andy Chen, achen93
#include <cmath>
#include "King.h"

/*
 * Indicates whether the given start and end coordinates describe
 * a move that would be considered valid for this piece if there
 * is no piece at the end position (i.e. this is not a capture move)
 * Makes sure the King is moving one in any direction.
 */
bool King::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  int start_col = start.first;
  int start_row = start.second;
  int end_col = end.first;
  int end_row = end.second; 
  double dx = abs(end_col - start_col);
  double dy = abs(end_row - start_row);
  //double hypo = sqrt(pow(dx,2) + pow(dy,2));

  // Traversing diagonally (but only 1 unit).
  if (dx == 1 && dy == 1) {
    return true;
  // Or traversing horizontally or verically (but only 1 unit).
  } else if ((dx == 1 && dy == 0) || (dx == 0 && dy == 1)) {
    return true;
  } else {
    return false;
  }
}

