// Nicolas Barrera, nbarrer1
// Rashi Bhatt, rbhatt6
// Andy Chen, achen93

#ifndef KING_H
#define KING_H

#include "Piece.h"

class King : public Piece {

public:
  // Checks if the move is a valid shape for the kind.
  bool legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const;

  /////////////////////////////////////
  // DO NOT MODIFY THIS FUNCTION!!!! //
  /////////////////////////////////////
  // Returns the ascii value of the King.
  char to_ascii() const {
    return is_white() ? 'K' : 'k';
  }


private:
  /////////////////////////////////////
  // DO NOT MODIFY THIS FUNCTION!!!! //
  /////////////////////////////////////
  // Checks if the King is white.
  King(bool is_white) : Piece(is_white) {}

  // Allows create_piece to create a dynamically allocated
  // king piece.
  friend Piece* create_piece(char piece_designator);
};

#endif // KING_H
