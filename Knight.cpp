// Nicolas Barrera, nbarrer1
// Rashi Bhatt, rbhatt6
// Andy Chen, achen93

#include "Knight.h"
#include <cmath>

/* 
 * Indicates whether the given start and end coordinates describe
 * a move that would be considered valid for this piece if there
 * is no piece at the end position (i.e. this is not a capture move)
 * Makes sure the knight is moving two forward/back and one left/right
 * and vice versa.
 */
bool Knight::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  // Define start and end positions and the distance between them.
  int start_col = start.first;
  int start_row = start.second;
  int end_col = end.first;
  int end_row = end.second;
  double dx = abs(end_col - start_col);
  double dy = abs(end_row - start_row);

  // If the knight is moving two up/down and one right/left, or vice
  // versa, return true.
  if ((dx == 2 && dy == 1) || (dx == 1 && dy == 2)) {
    return true;
  } else {
    return false;
  }
}