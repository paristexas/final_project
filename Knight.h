// Nicolas Barrera, nbarrer1
// Rashi Bhatt, rbhatt6
// Andy Chen, achen93

#ifndef KNIGHT_H
#define KNIGHT_H

#include "Piece.h"

class Knight : public Piece {

public:
  // Returns true if legal move shape for the Knight.
  bool legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const;
  
  /////////////////////////////////////
  // DO NOT MODIFY THIS FUNCTION!!!! //
  /////////////////////////////////////
  // Returns ascii value of the knight.
  char to_ascii() const {
    return is_white() ? 'N' : 'n';
  }


private:
  /////////////////////////////////////
  // DO NOT MODIFY THIS FUNCTION!!!! //
  /////////////////////////////////////
  // Returns whether the knight is white or not.
  Knight(bool is_white) : Piece(is_white) {}

  // Allows create_piece to dyanmically allocate a Knight.
  friend Piece* create_piece(char piece_designator);
};

#endif // KNIGHT_H
