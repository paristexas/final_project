# Nicolas Barrera, nbarrer1
# Rashi Bhatt, rbhatt6
# Andy Chen, achen93

CONSERVATIVE_FLAGS = -std=c++11 -Wall -Wextra -pedantic
DEBUGGING_FLAGS = -g -O0
CFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)

# Default target

chess: Chess.o main.o Board.o CreatePiece.o Rook.o Pawn.o Knight.o Bishop.o Queen.o King.o
	g++ -o chess Chess.o main.o Board.o CreatePiece.o Rook.o Pawn.o Knight.o Bishop.o Queen.o King.o

main.o: main.cpp Chess.h
	g++ -c main.cpp $(CFLAGS)

Chess.o: Chess.cpp Chess.h 
	g++ -c Chess.cpp $(CFLAGS)

Board.o: Board.cpp Board.h Chess.h CreatePiece.h Terminal.h
	g++ -c Board.cpp $(CFLAGS)

CreatePiece.o: CreatePiece.cpp CreatePiece.h Piece.h Pawn.h Rook.h Knight.h Bishop.h Queen.h King.h Mystery.h
	g++ -c CreatePiece.cpp $(CFLAGS)

Rook.o: Rook.cpp Rook.h
	g++ -c Rook.cpp $(CFLAGS)

Pawn.o: Pawn.cpp Pawn.h
	g++ -c Pawn.cpp $(CFLAGS)

Knight.o: Knight.cpp Knight.h
	g++ -c Knight.cpp $(CFLAGS)

Bishop.o: Bishop.cpp Bishop.h
	g++ -c Bishop.cpp $(CFLAGS)

Queen.o: Queen.cpp Queen.h
	g++ -c Queen.cpp $(CFLAGS)

King.o: King.cpp King.h
	g++ -c King.cpp $(CFLAGS)


.PHONY: clean
clean:
	rm -f *.o chess *~
