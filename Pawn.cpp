// Nicolas Barrera, nbarrer1
// Rashi Bhatt, rbhatt6
// Andy Chen, achen93

#include "Pawn.h"
#include <cmath>

// Indicates whether the given start and end coordinates describe
// a move that would be considered valid for this piece if there
// is no piece at the end position (i.e. this is not a capture move)
bool Pawn::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  // Define rows and columns and distances between them.
  int start_col = start.first;
  int start_row = start.second;
  int end_col = end.first;
  int end_row = end.second;
  int dx = end_col - start_col;
  int dy = end_row - start_row;

  // If pawn is white and is not moving columns,
  if (this->is_white() && dx == 0) {
    // return true if it going forward one
    if (dy == 1) {
      return true;
      // If it is at the starting position,
      // it can move two places too.
    } else if (start.second == '2' && dy == 2) {
      return true;
      // Otherwise, return false because it
      // doesn't meet any requirements.
    } else {
      return false;
    }
    // Do same for black pieces, but negate
    // dy because it going down the board.
  } else if (!this->is_white() && dx == 0) {
    if (dy == -1) {
      return true;
    } else if (start.second == '7' && dy == -2) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

/*
 * Indicates whether the given start and end coordinates describe a move
 * that would be considered valid for this pawn if there is a piece at the
 * end position (i.e. this is a capture move). Makes sure the capture
 * shape is diagonal by one position.
 */
bool Pawn::legal_capture_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  // Define rows and columns and distances between them.
  int start_col = start.first;
  int start_row = start.second;
  int end_col = end.first;
  int end_row = end.second;
  // Absolute value for dx because it can move right or left.
  int dx = abs(end_col - start_col);
  int dy = end_row - start_row;

  // If it's white and it's moving one column to the right or left
  if (this->is_white() && dx == 1) {
    // Return true only if it's also moving up by one as well
    if (dy == 1) {
      return true;
    } else {
      return false;
    }
    // Do same for black pieces, but negate dy.
  } else if (!this->is_white() && dx == 1) {
    if (dy == -1) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}
