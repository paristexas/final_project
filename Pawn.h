// Nicolas Barrera, nbarrer1
// Rashi Bhatt, rbhatt6
// Andy Chen, achen93

#ifndef PAWN_H
#define PAWN_H

#include "Piece.h"

class Pawn : public Piece {

public:
  virtual ~Pawn() {}

  // Returns true if the move is legal for a pawn.
  bool legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const;

  /////////////////////////////////////
  // DO NOT MODIFY THIS FUNCTION!!!! //
  /////////////////////////////////////
  // Returns the ascii value of the pawn.
  char to_ascii() const {
    return is_white() ? 'P' : 'p';
  }

  // Indicates whether the given start and end coordinates describe a move
  // that would be considered valid for this piece if there is a piece at the
  // end position (i.e. this is a capture move)
bool legal_capture_shape(std::pair<char, char> start, std::pair<char, char> end) const;

private:
  /////////////////////////////////////
  // DO NOT MODIFY THIS FUNCTION!!!! //
  /////////////////////////////////////
  // Returns whether the pawn is white.
  Pawn(bool is_white) : Piece(is_white) {}

  // Allows create_piece to dynamically a pawn.
  friend Piece* create_piece(char piece_designator);
};

#endif // PAWN_H
