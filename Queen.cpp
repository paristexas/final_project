// Nicolas Barrera, nbarrer1
// Rashi Bhatt, rbhatt6
// Andy Chen, achen93

#include "Queen.h"
#include <cmath>

/*
 * Indicates whether the given start and end coordinates describe
 * a move that would be considered valid for this piece if there
 * is no piece at the end position (i.e. this is not a capture move)
 * Returns true if moving linearly in any direction.
 */
bool Queen::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  // Define rows and columns and distances between them.
  int start_col = start.first;
  int start_row = start.second;
  int end_col = end.first;
  int end_row = end.second; 
  int dx = abs(end_col - start_col);
  int dy = abs(end_row - start_row);

  // Traversing horizontally or verically with no limits.
  if (start.first == end.first || start.second == end.second) {
    return true;
  // Traversing diagonally with no limits.
  } else if (dx == dy) {
    return true;
  } else {
    return false;
  }
}

