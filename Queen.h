// Nicolas Barrera, nbarrer1
// Rashi Bhatt, rbhatt6
// Andy Chen, achen93

#ifndef QUEEN_H
#define QUEEN_H

#include "Piece.h"

class Queen : public Piece {

public:
  // Returns true if the move shape is legal for the queen.
  bool legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const;

  /////////////////////////////////////
  // DO NOT MODIFY THIS FUNCTION!!!! //
  /////////////////////////////////////
  // Returns the ascii value of the Queen.
  char to_ascii() const {
    return is_white() ? 'Q' : 'q';
  }

private:
  /////////////////////////////////////
  // DO NOT MODIFY THIS FUNCTION!!!! //
  /////////////////////////////////////
  // Returns whether the Queen is white.
  Queen(bool is_white) : Piece(is_white) {}

  // Allows the create_piece to dynamically a Queen piece.
  friend Piece* create_piece(char piece_designator);
};

#endif // QUEEN_H
