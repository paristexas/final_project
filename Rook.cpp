// Nicolas Barrera, nbarrer1
// Rashi Bhatt, rbhatt6
// Andy Chen, achen93

#include "Rook.h"

/*
 * Indicates whether the given start and end coordinates describe
 * a move that would be considered valid for this piece if there
 * is no piece at the end position (i.e. this is not a capture move)
 * Makes sure the Rook is moving only forward/backward or only left/right.
 */
bool Rook::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  // Traversing horizontally or verically with no limits.
  if (start.first == end.first || start.second == end.second) {
    return true;
  } else {
    return false;
  }
}