// Nicolas Barrera, nbarrer1
// Rashi Bhatt, rbhatt6
// Andy Chen, achen93

#ifndef ROOK_H
#define ROOK_H

#include "Piece.h"

class Rook : public Piece {

public:
  // Returns true if the move is legal for a Rook.
  bool legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const;

  /////////////////////////////////////
  // DO NOT MODIFY THIS FUNCTION!!!! //
  /////////////////////////////////////

  // Returns the ascii value of the rook.
  char to_ascii() const {
    return is_white() ? 'R' : 'r';
  }

private:
  /////////////////////////////////////
  // DO NOT MODIFY THIS FUNCTION!!!! //
  /////////////////////////////////////
  // Returns whether the Rook is white or not.
  Rook(bool is_white) : Piece(is_white) {}

  // Allows create_piece to dynamically allocate a Rook piece.
  friend Piece* create_piece(char piece_designator);
};

#endif // ROOK_H
